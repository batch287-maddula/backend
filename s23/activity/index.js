let trainer ={
    name: "Ash Ketchum",
    age : 10,
    pokemon:["Pikachu", "Charizard", "Squirtle" , "Bulbasaur"],
    friends: {let :friend ={
        hoenn : ["May","Max"],
        kanto : ["Brock","Misty"]
    }},
    talk: function () {
        console.log("Result of talk method: ");
        console.log("Pikachu! I chose you!!");
    }
}
console.log(trainer);
console.log("Result of dot notation: ");
console.log(trainer.name);

console.log("Result of square bracket notation: ");
console.log(trainer["pokemon"]);


trainer.talk();

function Pokemon (name, level){
    this.name = name;
    this.level = level;
    this.health = 2*level;
    this.attack= level;

    this.tackle = function (targetPokemon) {
        targetPokemon.health -= this.attack;
        console.log(this.name + " tackled " + targetPokemon.name);
		console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health);

        if (targetPokemon.health<=0) {
            console.log(targetPokemon.name+" fainted");
        }
        console.log(targetPokemon);
        }
};

let pikachu = new Pokemon ('Pikachu', 12);
	console.log(pikachu)
let geodude = new Pokemon ('Geodude', 8);
	console.log(geodude)
let mewtwo = new Pokemon ('Mewtwo', 100);
	console.log(mewtwo)


geodude.tackle(pikachu);

mewtwo.tackle(geodude);
