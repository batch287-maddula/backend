console.log(
    "Hello World"
);

let studentNumberA= '2020-1923';
let studentNumberB= '2020-1924';
let studentNumberC= '2020-1925';
let studentNumberD= '2020-1926';
let studentNumberE= '2020-1927';

let studentNumbers = ['2020-1923','2020-1924','2020-1925','2020-1926','2020-1927'];

console.log(studentNumbers);

let grades = [98.5 , 94.5 , 78.7, 88.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba' , 'Fujitsu']

let mixedArr = [12, 'Asus', true , null, undefined ,{}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

let myTasks = ['drink HTML', 'eat JavaScript', 'inhale CSS' ,'Bake Bootstrap'];

let city1 = 'Japan';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1 , city2 ,city3];

console.log(myTasks);
console.log(cities);

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);
let fullName = 'Zayn Malik'
console.log(fullName.length);

myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);



cities.length--;
console.log(cities);

fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
console.log(theBeatles);
theBeatles.length++;
console.log(theBeatles);

console.log(grades[3]);
console.log(computerBrands[2]);
console.log(grades[100]);



let lakersLegends = [ 'Kobe', 'Shaq', 'LeBron', 'Magic', 'Kareem' ];

console.log(lakersLegends[1]); // Will print Shaq
console.log(lakersLegends[3]); // Will print Magic

// We can also save/store array items in another variable.
let currentLaker = lakersLegends[2];
console.log(currentLaker); // Will show Lebron

console.log('Array before reassignment');
console.log(lakersLegends);
lakersLegends[2] = 'Pau Gasol';
console.log('Array after reassignment');
console.log(lakersLegends);

// Accessing the last element of an array
// Since the first elemet of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element

let bullsLegends = [ 'Jordan', 'Pippen', 'Rodman', 'Rose', 'Boozer','Kukoc' ];
console.log(bullsLegends);

let lastElementIndex = bullsLegends.length - 1;

console.log(bullsLegends[lastElementIndex]);

// You can also add it directly:
console.log(bullsLegends[bullsLegends.length-1]);

// Adding Items into the Array

    let newArr = [];
    console.log(newArr[0]); // undefined

    newArr[0] = 'Cloud Strife';
    console.log(newArr);

    console.log(newArr[1]);
    newArr[1] = 'Tifa Lockhart';

    console.log(newArr);

    // newArr[newArr.length-1] = 'Barrett Wallace'

    newArr[newArr.length] = 'Barrett Wallace';
    console.log(newArr)

// Looping Over an Array

// We can use For Loop to iterate over all items in an array.

    for(let index = 0; index < newArr.length; index++){

        // You can use the loop counter as index to be able to show each array items in a console log. 
        console.log(newArr[index]);
    };


let numArr = [ 5, 12, 69, 87, 765 ];

for(let index = 0; index < numArr.length; index++){

    if(numArr[index] % 5 === 0){

        console.log(numArr[index] + " is divisible by 5");

    } else {

        console.log(numArr[index] + " is not divisible by 5");

    }
}

// [ SECTION ] Multidimensional Arrays

// Multidimensional Arrays are useful for storing complex data structures

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
];

console.log(chessBoard);

// Accessing elements of a multidimensional arrays
console.log(chessBoard[1][7]);

console.log('Pawn moves to: ' + chessBoard[1][5]);
