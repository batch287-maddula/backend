// CRUD Operations
    // CRUD Operations are the heart of any backend application.
    // Mastering the CRUD Operations is essential for any developer.

// [ SECTION ] Inserting Documents (CREATE)

// Insert One Documents
    // Creating MongoDB Syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly.
    // Syntax:
        // db.collectionName.insertOne({object})

        db.users.insertOne({
            "firstName": "John",
            "lastName": "Smith"
        });
        db.users.insertOne({
            firstName: "Jane",
            lastName: "Doe",
            age: 21,
            contact: {
                phone: "1234567890",
                email: "janedoe@mail.com"
            },
            courses: ["CSS", "Javascript", "Python"],
            departent: "none"
        });

//  Insert Many Document

    // Syntax
        // db.collectionName.insertMany([ {objectA}, {objectB} ]);


        db.users.insertMany([
            {
                firstName: "Stephen",
                lastName: "Hawkin",
                age: 76,
                contact: {
                    phone: "1234567890",
                    email: "theoryofeverything@mail.com"
                },
                courses: ["Python", "React", "PHP"],
                departent: "none"
            },
            {
                firstName: "Neil",
                lastName: "Armstrong",
                age: 82,
                contact: {
                    phone: "1234567890",
                    email: "firstmaninthemoon@mail.com"
                },
                courses: ["React", "Laravel", "Sass"],
                departent: "none"
            }
        ]);

// [ SECTION ] Finding Documents (READ)

//     Find
//         If multiple documents match the criteria for finding a document only for First document that matches he search term will be returned

//     Syntax:
            db.collectionName.find();
            db.collectionName.find({ field: "value" });

        db.collectionName.find();
        db.collectionName.find({ firstName: "Stephen" });

// [ SECTION ] Updating Documents (UPDATE)

//     Updating a single document
        db.users.insertOne({
            firstName: "Test",
            lastName: "Test",
            age: 0,
            contact: {
                phone: "0000000000",
                email: "test@mail.com"
            },
            courses: [],
            departent: "none"
        });

    // To update firstName: "Test"
    // Syntax:
    db.collectionName.updateOne( {criteria}, { $set: { field: value }});


        db.users.updateOne(
            {firstName: "Test"},
            {
                $set: {
                firstName: "Bill",
                lastName: "Gates",
                age: 65,
                contact: {
                    phone: "0987654321",
                    email: "billgates@mail.com"
                },
                courses: ["PHP", "C++", "HTML"],
                departent: "Operations",
                status: "active"
            }
            }
        );


    // Update Many

    // Syntax:
    db.collectionName.updateMany( {criteria}, { $set: { field: value }});


        db.users.updateMany(
            {departent: "none"},
            {
                $set: {
                departent: "HR",
            }
            }
        );


 // [ SECTION ] Deleting Documents (DELETE)

    // Delete One

        // Syntax:
        db.collectionName.deleteOne({ criteria });

       
        db.users.insertOne({
            "firstName": "Test",
        });

        db.users.deleteOne({
            "firstName": "Test",
        });


 // Delete Many
        // Syntax:
        db.collectionName.deleteMany({ criteria });

        db.users.deleteMany({
            departent: "HR",
        });

        db.users.updateMany(
            {departent: "none"},
            {
                $set: {
                departent: "HR",
            }
            }
        );