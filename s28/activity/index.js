db.hotel.insertOne({
    name : "Single",
    accommodates : 2,
    price : 1000,
    description : "A simple room witth all basic necessities",
    rooms_available :10,
    isAvailable : "false"
});

db.hotel.insertMany([
    {
    name : "Double",
    accommodates : 3,
    price : 2000,
    description : "A room fit for a small family going on a vacation",
    rooms_available :5,
    isAvailable : "false"
    },
    {
    name : "Queen",
    accommodates : 4,
    price : 4000,
    description : "A room with queen sized bed perfect for a simple getaway",
    rooms_available :15,
    isAvailable : "false"
    }
]);



db.hotel.find(
    {name: "Double"}
);



db.hotel.updateOne(
    {name : "Queen"},
    {
        $set: {
            rooms_available : 0
        }
});



db.hotel.deleteMany(
    {rooms_available : 0}
);