
var getCube = 2;
console.log(`The cube of ${getCube} is ${getCube**3}`);

let address= [258, "Washington Ave", "NW" , "California", 90011 ];

const [door ,street,city,country,pincode] = address;
console.log(`I live at ${door} ${street} ${city}, ${country} ${pincode}`);

let animal ={
    name : "Lolong",
    species: "salt water Crocodile",
    weight: "1075 Kgs",
    dimensions: "20ft 3in"
}

const {name ,species , weight , dimensions} = animal;
console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${dimensions}.`);

let numbers =[1,2,3,4,5];
numbers.forEach(element => {
    console.log(element);
});

let reduceNumbers = numbers.reduce((a,b) => a+b);
console.log(reduceNumbers);


class dog{
    constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
}
}

const newDog = new dog("Frankie",5,'Miniature Dashshund');

console.log(newDog);