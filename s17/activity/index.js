/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function bioData(){
        let fullname=prompt('Enter your full name: ')
        let age=prompt('Enter your age: ')
        let city=prompt('Enter your city: ')
        console.log("Hello, "+ fullname);
        console.log("You are "+ age + " years old");
        console.log("You live in "+ city + " City");
    }
    bioData();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    function MyFavBands(){
        console.log("1. One Direction");
        console.log("2. Zayn Malik");
        console.log("3. Taylor Shift");
        console.log("4. Selena Gomez");
        console.log("5. The Weeknd");
    }
    MyFavBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function MyFavMovies(){
        console.log("1. Avengers EndGame");
        console.log("Rotten Tomatoes Rating :94%");
        console.log("2. Interstellar");
        console.log("Rotten Tomatoes Rating :73%");
        console.log("3. Spider Man: Across No Way Home");
        console.log("Rotten Tomatoes Rating :93%");
        console.log("4. John Wick : 4");
        console.log("Rotten Tomatoes Rating :94%");
        console.log("5. Spider Man: Across the Spider Verse");
        console.log("Rotten Tomatoes Rating :96%");
    }
    MyFavMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


 function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
   

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();
