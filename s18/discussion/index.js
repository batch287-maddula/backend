console.log("Hello World!!");

// [SECTION] Functions
    // [SUBSECTION] Parameters and Arguments
    
    // Functions in JS are lines/blocks of code that will tell our device/application/browser to perform a certain task when called/invoked/triggered
    function printInput(){
        let nickname= prompt("Enter your Nickname: ");
        console.log("Hi " + nickname);
    }
   // printInput();

	// For other cases, functions can also process data directly passed into it instead of relying only on Global Variable and prompt().

	// name ONLY ACTS LIKE A VARIABLE


    function printName(name){// name is the parameter
			
        // This one should work as well:
            // let name = name;
            // console.log("Hi, " + name);

        console.log("Hi, " + name);
        }
			//console.log(name); // This one is error as it is not defined.


    printName("Vighnu");
    printName("Huehue");

    let sampleVariable="Yui";
    printName (sampleVariable);

		// Function arguements cannot be used by a function if there are no parameters provided within the function.

		printName(); // It will be Hi!, undefiend


    function checkDivisibilityby8 (num){
        let remainder = num%8
        console.log("The remainder of " + num + " divided by 8 is : " + remainder);
        let isDivisibleby8= remainder ===0;
        console.log("Is " +num + " divisible by 8?");
        console.log(isDivisibleby8);
    }
    checkDivisibilityby8(32);
    checkDivisibilityby8(52);
	// You can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathemical computations.


	// Functions as Arguements

		// Function parameters can also accpet other function as arguements


    function arguementFunction(){
        console.log("This function was passed as an arguement before the message was printed.")
    };

    // function invokeFunction(arguementFunction){
    // 	arguementFunction();
    // };

    function invokeFunction(iAmNotRelated){
        //console.log(iAmNotRelated);
        iAmNotRelated();
        arguementFunction();
    };

    invokeFunction(arguementFunction);

    // Adding and removing the parenthese "()" impacts the output of JS heavily.

    console.log(arguementFunction);
    // Will provide information about a function in the console using console.log();



	// Using multiple parameters

		// Mutiple "arguements" will correspond to the number of "parameters" declared in a function in succeding order. 

		function createFullName(firstName, middleName, lastName){
			console.log( firstName + " " + middleName + " " + lastName);

			console.log( middleName + " " + lastName + " " + firstName);
		};

		createFullName("Mike", "Kel", "Jordan");

		createFullName("Mike","Jordan");

		createFullName("Mike", "Kel", "Jordan", "Mike");

		// In JS, providing more/less arguements than the expected parameters will not return an error.

		// Using Variable as Arguments

		let firstName = "Dwayne";
		let secondName = "The Rock";
		let lastName = "Johnson";

		createFullName(firstName, secondName, lastName);

// [ SECTION ] The Return Statement

	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

		function returnFullName(firstName, middleName, lastName){
			console.log("This message is from console.log");
			return firstName + " " + middleName + " " + lastName;
			console.log("This message is from console.log");
			console.log("This message is from console.log");
			console.log("This message is from console.log");
			console.log("This message is from console.log");
			console.log("This message is from console.log");
			console.log("This message is from console.log");
			console.log("This message is from console.log");
		};

		let completename = returnFullName("Jeffrey", "Smith", "Bezos");
		console.log(completename);

		// In our example, the "returnFullName" function was invoked called in the same line as declaring a variable.

		// Whatever value is returned from the "returnFullName" function is stored in the "completeName" variable.

		// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statment is ignored because it ends the function execution.

		// In this example, console.log() will print the returned value of the returnFullName() function.
		console.log(returnFullName(firstName, secondName, lastName));

		// You can also create a variable inside the function to contain the result and return that variable instead.

		function returnAddress(city, country){
			let fullAddress = city + ", " + country;
			return fullAddress;
		};

		let myAddress = returnAddress("Vizag City" , "India");
		console.log(myAddress);

		// On the other hand, when a function the only has console.log() to display its result will be undefined insted.


    function printPlayerInfo(username, level, job){
        console.log("Username: " + username);
        console.log("Level: " + level);
        console.log("Job: " + job);
    };

    let user1 = printPlayerInfo("knight_white", 95, "Paladin");
    console.log(user1);

    // Returns undefined because printPlayerInfo return nothing. It only console.log the details.

    // You cannot save any value from printPlayerInfo() because it does not return anything. 
