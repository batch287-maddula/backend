// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require ("./routes/taskRoute")
const app = express();
const port = 5001;
// Setup the server
app.use(express.json());
app.use(express.urlencoded({extended: true}));
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.qatv1d1.mongodb.net/s35-discussion",
{
    useNewUrlParser : true,
    useUnifiedTopology :true
});

// Setup the Database
mongoose.connection.once("open", ()=>console.log("We're connected to cloud database"));

app.listen(port,() => console.log(`Server running at port ${port}`));
// Add the tasks route
// Allow us to all the task routes created in "taskRoute.js" file to use "/tasks" route
	// http://localhost:5001/tasks
app.use("/tasks",taskRoute);
